export ZSH="$HOME/.oh-my-zsh"
export ZSH_CUSTOM=$HOME/.shell

# dotfiles settings
if [ -f "$HOME/.dotfiles_settings" ]; then
  source "$HOME/.dotfiles_settings"
fi
export DOTFILES_PROFILE=${DOTFILES_PROFILE:-default}

# Theme
ZSH_THEME="kb9zzw"

# Workspaces
export WORKSPACE=$HOME/workspace

# No beeps
unsetopt BEEP

# Autoload compinit
autoload -U compinit
compinit

fpath=($ZSH_CUSTOM/functions $fpath)
autoload -U $ZSH_CUSTOM/functions/*(:t)

# Load oh-my-zsh
source $ZSH/oh-my-zsh.sh

# ZSH includes
ZSH_INCLUDES=$ZSH_CUSTOM/includes

while read -r file; do
  source "$file"
done < <(find ${ZSH_INCLUDES} -type f)

# Include local definitions, if available
for profile in $DOTFILES_PROFILE local; do
  if [ -f "$HOME/.zshrc.$profile" ]; then
    source "$HOME/.zshrc.$profile"
  fi
done

# Plugins
plugins=(
  git
  virtualenv
  fzf
  rtx
)

# Starship
if (( $+commands[starship] )); then
  eval "$(starship init zsh)"
fi
