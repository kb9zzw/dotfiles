# .bashrc

# Prefer zsh if available
if [ -z "${NOZSH}" ] && [ $TERM = "xterm" -o $TERM = "xterm-256color" -o $TERM = "screen" ] && type zsh &> /dev/null
then
    export SHELL=$(which zsh)
    if [[ -o login ]]
    then
        exec zsh -l
    else
        exec zsh
    fi
fi

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Source dotfiles settings
if [ -f "$HOME/.dotfiles_settings" ]; then
  . $HOME/.dotfiles_settings
fi
export DOTFILES_PROFILE=${DOTFILES_PROFILE:-default}

# Source local definitions
for profile in $DOTFILES_PROFILE local; do
  if [ -f "$HOME/.bashrc.$profile" ]; then
    . $HOME/.bashrc.$profile
  fi
done

# If not running interactively, don't do anything else
[[ $- != *i* ]] && return

# Source bash_profile
if [ -f $HOME/.bash_profile ]; then
  . $HOME/.bash_profile
fi
