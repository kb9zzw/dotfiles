$global:DOTFILES_SRC = "https://gitlab.com/kb9zzw/dotfiles.git"

Function dotfiles() {
  write-host $args
  git --git-dir=$HOME\.dotfiles --work-tree=$HOME $args
}

Function install() {
  if (!(Test-Path -Path $HOME\.dotfiles)) {
    git clone --bare $global:DOTFILES_SRC $HOME\.dotfiles
    dotfiles config --local core.sparseCheckout true
    dotfiles config --local status.showUntrackedFiles no
    $NoSparse = @"
!LICENSE
!README.md
!install*
!dotfiles.*cfg
!.b*
!.s*
!.z*
"@
    Write-Output $NoSparse > $HOME\.dotfiles\info\sparse-checkout
    dotfiles checkout -f

    Copy-Item -Path "$HOME\.profile.ps1" -Destination "$profile" -Force
  }
}

install
