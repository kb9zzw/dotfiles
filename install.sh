#!/bin/bash

# default settings
DOTFILES_PROFILE=${DOTFILES_PROFILE:-default}
DOTFILES_SRC=${DOTFILES_SRC:-https://gitlab.com/kb9zzw/dotfiles.git}
DOTFILES_BRANCH=${DOTFILES_BRANCH:-main}

# Fuzzy Find
FZF_ENABLED=${FZF_ENABLED:-1}
FZF_SRC=https://github.com/junegunn/fzf.git

# Oh My ZSH
OMZ_ENABLED=${OMZ_ENABLED:-1}
OMZ_SRC=${OMZ_SRC:-https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh}

# ASDF
ASDF_ENABLED=${ASDF_ENABLED:-1}
ASDF_SRC=${ASDF_SRC:-https://github.com/asdf-vm/asdf.git}
ASDF_BRANCH=${ASDF_BRANCH:-master}
ASDF_PLUGINS=${ASDF_PLUGINS:-python terraform java nodejs kubectl helm cf oc}

banner() {
cat <<"EOF"

   __| | ___ | |_ / _(_) | ___  ___ 
  / _` |/ _ \| __| |_| | |/ _ \/ __|
 | (_| | (_) | |_|  _| | |  __/\__ \
  \__,_|\___/ \__|_| |_|_|\___||___/

EOF
}

dotfiles() {
  git --git-dir=$HOME/.dotfiles --work-tree=$HOME $@
}

error_exit() {
  echo $1
  exit ${2:-1}
}

preflight() {
  command -v git > /dev/null || error_exit "ERROR: git not found, install git and try again."
  command -v curl > /dev/null || error_exit "ERROR: curl not found, install curl and try again."
}

install() {
  preflight
  if [ ! -d $HOME/.dotfiles ]; then
    git clone --bare $DOTFILES_SRC $HOME/.dotfiles
    dotfiles config --local core.sparseCheckout true
    dotfiles config --local status.showUntrackedFiles no
    cat > $HOME/.dotfiles/info/sparse-checkout <<EOF
/*
!LICENSE
!README.md
!install*
!dotfiles.*cfg
!**/*.ps1
EOF

    # exclude macosx only config
    if [[ $OSTYPE != darwin* ]]; then
      cat >> $HOME/.dotfiles/info/sparse-checkout << EOF
!Library
!.iterm2
EOF
    fi
  fi
  dotfiles checkout -f

  # dotfiles settings
  echo "export DOTFILES_PROFILE=$DOTFILES_PROFILE" > $HOME/.dotfiles_settings
}

# initialize fuzzy find
init_fzf() {
  if [ $FZF_ENABLED -eq 0 ]; then return 0; fi
  if [ ! -d "$HOME/.fzf" ]; then
    git clone --depth 1 "$FZF_SRC" $HOME/.fzf
    $HOME/.fzf/install --all --no-update-rc
  fi
}

# initialize oh my zsh
init_omz() {
  if [ $OMZ_ENABLED -eq 0 ]; then return 0; fi
  if [ ! -d "$HOME/.oh-my-zsh" ]; then
    export KEEP_ZSHRC="yes"
    export RUNZSH="no"
    export CHSH="no"
    sh -c "$(curl -fsSL $OMZ_SRC)"
  fi
}

# initialize asdf
init_asdf() {
  if [ $ASDF_ENABLED -eq 0 ]; then return 0; fi
  if [ ! -d "$HOME/.asdf" ]; then
    git clone $ASDF_SRC $HOME/.asdf --branch $ASDF_BRANCH
  fi

  # install plugins
  source $HOME/.asdf/asdf.sh
  for plugin in $ASDF_PLUGINS; do
    asdf plugin add $plugin
  done
}

# initialze vim
init_vim() {
  if command -v vim &> /dev/null && \
    ! [ -d "$HOME/.vim/bundle/Vundle.vim" ]; then
    vim || [ -d "$HOME/.vim/bundle/Vundle.vim" ]
  fi
}

# initialize vscode
init_vscode() {
  if command -v code &> /dev/null && \
    ! [ -d "$HOME/.vscode/extensions" ]; then
    $HOME/.local/bin/vscode_extensions 
  fi
}

main() {
  if [ -e "$DOTFILES_CFG" ]; then
    source $DOTFILES_CFG
  elif [ -e "dotfiles.$PROFILE.cfg" ]; then
    source dotfiles.$PROFILE.cfg
  else 
    true
  fi

  install
  source $HOME/.dotfiles_settings

  init_fzf
  init_omz
  init_vim
  init_asdf
  init_vscode
  banner
  echo "Installed!"
}

main
