#!/bin/env bash

# Include bash config
BASH_INCLUDES=~/.shell/includes
while read -r file; do
  source "$file"
done < <(find ${BASH_INCLUDES} -type f)

# Source local definitions, if available
for profile in $DOTFILES_PROFILE local; do
  if [ -f "$HOME/.bash_profile.$profile" ]; then
    . $HOME/.bash_profile.$profile
  fi
done

# Starship
if command -v starship > /dev/null 2>&1; then
  eval "$(starship init bash)"
fi
