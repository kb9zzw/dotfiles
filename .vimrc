set nocompatible
filetype off

" source if file exists
function! SourceIfExists(file)
  if filereadable(expand(a:file))
    execute 'source '.a:file
  endif
endfunction

" profile-specific config
let g:profile = $DOTFILES_PROFILE

" load profile-specific config
call SourceIfExists('~/.vim/settings/'.g:profile.'.vim')

" load local config, if exists
call SourceIfExists('~/.vim/settings/local.vim')
